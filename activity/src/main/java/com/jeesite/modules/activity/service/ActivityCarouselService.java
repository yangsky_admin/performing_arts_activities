/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.service;

import java.util.List;

import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.utils.ResultUtil;
import com.jeesite.modules.sys.utils.DictUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.activity.entity.ActivityCarousel;
import com.jeesite.modules.activity.dao.ActivityCarouselDao;

/**
 * activity_carouselService
 * @author ReStartLin
 * @version 2018-09-28
 */
@Service
@Transactional(readOnly=true)
public class ActivityCarouselService extends CrudService<ActivityCarouselDao, ActivityCarousel> {



	/**
	 * 获取单条数据
	 * @param activityCarousel
	 * @return
	 */
	@Override
	public ActivityCarousel get(ActivityCarousel activityCarousel) {
		return super.get(activityCarousel);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param activityCarousel
	 * @return
	 */
	@Override
	public Page<ActivityCarousel> findPage(Page<ActivityCarousel> page, ActivityCarousel activityCarousel) {
		return super.findPage(page, activityCarousel);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param activityCarousel
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(ActivityCarousel activityCarousel) {
		super.save(activityCarousel);
	}
	
	/**
	 * 更新状态
	 * @param activityCarousel
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(ActivityCarousel activityCarousel) {
		super.updateStatus(activityCarousel);
	}
	
	/**
	 * 删除数据
	 * @param activityCarousel
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(ActivityCarousel activityCarousel) {
		super.delete(activityCarousel);
	}

}