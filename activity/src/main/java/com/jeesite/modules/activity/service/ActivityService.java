/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.service;

import java.util.List;

import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.utils.ResultUtil;
import com.jeesite.modules.sys.utils.DictUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.activity.entity.Activity;
import com.jeesite.modules.activity.dao.ActivityDao;

/**
 * activityService
 * @author ReStartLin
 * @version 2018-09-28
 */
@Service
@Transactional(readOnly=true)
public class ActivityService extends CrudService<ActivityDao, Activity> {



	/**
	 * 获取单条数据
	 * @param activity
	 * @return
	 */
	@Override
	public Activity get(Activity activity) {
		return super.get(activity);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param activity
	 * @return
	 */
	@Override
	public Page<Activity> findPage(Page<Activity> page, Activity activity) {
		return super.findPage(page, activity);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param activity
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Activity activity) {
		super.save(activity);
	}
	
	/**
	 * 更新状态
	 * @param activity
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Activity activity) {
		super.updateStatus(activity);
	}
	
	/**
	 * 删除数据
	 * @param activity
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Activity activity) {
		super.delete(activity);
	}

}