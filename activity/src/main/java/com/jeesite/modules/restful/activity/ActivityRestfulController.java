package com.jeesite.modules.restful.activity;

import com.jeesite.common.bean.response.activity.AcResponse;
import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.bean.result.ResultEnum;
import com.jeesite.common.entity.Page;
import com.jeesite.common.utils.ResultUtil;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.activity.entity.Activity;
import com.jeesite.modules.activity.service.ActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.jeesite.common.utils.ResultUtil.successList;

@RestController
@RequestMapping("/interface/activity")
public class ActivityRestfulController extends BaseController {
    public static final Logger logger = LoggerFactory.getLogger(ActivityRestfulController.class);

    private final ActivityService activityService;

    @Autowired
    public ActivityRestfulController(ActivityService activityService) {
        this.activityService = activityService;
    }

    /**
     * 获得所有的活动信息 status = 0
     * @return
     */
    @GetMapping("/list")
    public ResultBean queryAll() {
        Activity activity = new Activity();
        List<Activity> list = activityService.findList(activity);
        return successList(new AcResponse().doCopiers(list));
    }
    /**
     * 分页查询获得所有的活动信息 status = 0
     * pageIndex:第几页 从1开始
     * size:分页长度
     * @return
     */
    @GetMapping("/list/{pageIndex}/{size}")
    public ResultBean queryActivities(@PathVariable("pageIndex")Integer pageIndex,@PathVariable("size")Integer size) {
        Activity activity = new Activity();
        Page<Activity> page = activityService.findPage(new Page<>(pageIndex, size), activity);
        if (pageIndex > page.getLast()) {
            return successList(ResultEnum.PAGE_EXCEPTION,new ArrayList<>());
        }
        List<Activity> list = page.getList();
        return successList(new AcResponse().doCopiers(list));
    }

    /**
     * 增加阅读次数
     * @param id
     */
    @GetMapping("/{activityId}/hits")
    public ResultBean addHits(@PathVariable("activityId")String id) {
        Activity activity = activityService.get(id);
        if (activity != null) {
            Long activityHits = activity.getActivityHits();
            activity.setActivityHits(++activityHits);
            activityService.save(activity);
        }
        return ResultUtil.success();

    }


    @GetMapping("test")
    public ResultBean test() {
        return ResultUtil.success();
    }
}

