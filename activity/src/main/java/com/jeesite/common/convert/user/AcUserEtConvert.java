package com.jeesite.common.convert.user;

import com.jeesite.modules.sys.utils.DictUtils;
import org.springframework.cglib.core.Converter;

public class AcUserEtConvert implements Converter {
    public static final String AC_USER_SEX = "ac_user_sex";
    @Override
    public Object convert(Object value, Class aClass, Object o1) {
        switch (o1.toString()) {
            case "setGender":
                return DictUtils.getDictLabel(AC_USER_SEX, value.toString(), "未知");
        }
        return value;
    }
}
