package com.jeesite.common.bean.user;

import com.jeesite.modules.user.entity.ActivityUser;

import javax.validation.constraints.NotBlank;


public class UserLoginBean {
    @NotBlank(message = "openid不能为空")
    private String openid;
    private ActivityUser user;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public ActivityUser getUser() {
        return user;
    }

    public void setUser(ActivityUser user) {
        this.user = user;
    }
    @Override
    public String toString() {
        return "UserLoginBean{" +
                "openid='" + openid + '\'' +
                ", user=" + user +
                '}';
    }
}
