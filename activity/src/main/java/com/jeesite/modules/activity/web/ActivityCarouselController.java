/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.activity.entity.Activity;
import com.jeesite.modules.activity.entity.ActivityContent;
import com.jeesite.modules.activity.service.ActivityContentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.activity.entity.ActivityCarousel;
import com.jeesite.modules.activity.service.ActivityCarouselService;

import java.util.List;

/**
 * activity_carouselController
 * @author ReStartLin
 * @version 2018-09-28
 */
@Controller
@RequestMapping(value = "${adminPath}/activity/activityCarousel")
public class ActivityCarouselController extends BaseController {

	private final ActivityCarouselService activityCarouselService;
	private final ActivityContentService activityContentService;

	@Autowired
	public ActivityCarouselController(ActivityCarouselService activityCarouselService, ActivityContentService activityContentService) {
		this.activityCarouselService = activityCarouselService;
		this.activityContentService = activityContentService;
	}

	/**
	 * 获取数据
	 */
	@ModelAttribute
	public ActivityCarousel get(String id, boolean isNewRecord) {
		return activityCarouselService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("activity:activityCarousel:view")
	@RequestMapping(value = {"list", ""})
	public String list(ActivityCarousel activityCarousel, Model model) {
		model.addAttribute("activityCarousel", activityCarousel);
		return "modules/activity/activityCarouselList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("activity:activityCarousel:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<ActivityCarousel> listData(ActivityCarousel activityCarousel, HttpServletRequest request, HttpServletResponse response) {
		Page<ActivityCarousel> page = activityCarouselService.findPage(new Page<ActivityCarousel>(request, response), activityCarousel); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("activity:activityCarousel:view")
	@RequestMapping(value = "form")
	public String form(ActivityCarousel activityCarousel, Model model) {
		model.addAttribute("activityCarousel", activityCarousel);
		String activityContentId = activityCarousel.getActivityContentId();
		model.addAttribute("activityCarouselContent", "未编辑");
		if (!StringUtils.isEmpty(activityContentId)) {
			//根据内容ID获得富文本框的内容 -- carousel
			ActivityContent activityContent = activityContentService.get(activityContentId);
			if (activityContent != null) {
				String content = activityContent.getActivityContent();
				model.addAttribute("activityCarouselContent",content );
			}
		}
		return "modules/activity/activityCarouselForm";
	}

	/**
	 * 保存activity_carousel
	 */
	@RequiresPermissions("activity:activityCarousel:edit")
	@PostMapping(value = "save")
	@ResponseBody
	@Transactional
	public String save(HttpServletRequest request,@Validated ActivityCarousel activityCarousel) {
        String contentEditor = request.getParameter("contentEditor");
        if (!StringUtils.isEmpty(contentEditor)){
            boolean flag = false;//是否是新的  true  是
            ActivityContent activityContent = new ActivityContent();
            activityContent.setActivityContent(contentEditor);
            String activityContentId = activityCarousel.getActivityContentId();
            if (!StringUtils.isEmpty(activityContentId)) {
                activityContent.setId(activityContentId);
                flag = true;
            }
            activityContentService.save(activityContent);
            if (!flag) {
                activityCarousel.setActivityContentId(activityContent.getId());
            }
        }
        Long weight = activityCarousel.getWeight();
        if (weight == null) {
            Page<ActivityCarousel> page = activityCarouselService.findPage(new Page<>(1, 1), new ActivityCarousel());
            if (page != null) {
                List<ActivityCarousel> list = page.getList();
                if (list != null && list.size() > 0) {
                    activityCarousel.setWeight(list.get(0).getWeight()+1);
                }
            }
        }
        activityCarouselService.save(activityCarousel);
        return renderResult(Global.TRUE, text("保存成功！"));
	}
	
	/**
	 * 删除activity_carousel
	 */
	@RequiresPermissions("activity:activityCarousel:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(ActivityCarousel activityCarousel) {
		activityCarouselService.delete(activityCarousel);
		return renderResult(Global.TRUE, text("删除activity_carousel成功！"));
	}
	
}