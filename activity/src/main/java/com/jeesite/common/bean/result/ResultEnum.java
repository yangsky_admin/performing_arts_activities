package com.jeesite.common.bean.result;

public enum ResultEnum {
    SUCCESS(200, "成功"),
    PAGE_EXCEPTION(401, "已经是最后一页了"),
    ERROR(201, "失败"),
    ERROR_VIOLATION(202, "参数校验失败"),
    ERROR_UNKNOWN(204, "未知错误"),
    LOGIN_ERROR(201, "登录失败,用户不存在"),
    USER_ERROR(201, "用户不存在"),;

    private Integer code;//状态码
    private String msg;//信息
    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
