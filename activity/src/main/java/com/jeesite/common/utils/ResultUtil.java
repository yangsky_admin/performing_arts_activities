package com.jeesite.common.utils;


import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.bean.result.ResultEnum;

import java.util.Collection;

public class ResultUtil {

    public static ResultBean success(int code,String msg,Object data,Integer total) {
        return new ResultBean(code,msg,data,false,total);
    }
    public static ResultBean success(ResultEnum resultEnum, Object data,Integer total) {
        return success(resultEnum.getCode(),resultEnum.getMsg(),data,total);
    }
    public static ResultBean success(String msg,Object data,Integer total) {
        return success(ResultEnum.SUCCESS.getCode(), msg, data,total);
    }
    public static ResultBean success(String msg) {
        return success( msg, null,null);
    }
    public static ResultBean success() {
        return success("成功");
    }

    /**
     * 错误
     */

    public static ResultBean error(int code,String msg,Object data,Integer total) {
        return new ResultBean(code,msg,data,true,total);
    }
    public static ResultBean error(ResultEnum resultEnum, Object data,Integer total) {
        return error(resultEnum.getCode(),resultEnum.getMsg(),data,total);
    }
    public static ResultBean error(ResultEnum resultEnum) {
        return error(resultEnum.getCode(),resultEnum.getMsg(),null,0);
    }
    public static ResultBean error(String msg,Object data,Integer total) {
        return error(ResultEnum.ERROR.getCode(), msg, data,total);
    }
    public static ResultBean error(String msg) {
        return error( msg, null,null);
    }
    public static ResultBean error() {
        return error("失败");
    }

    /**返回body*/
    public static ResultBean successBody(Object data) {
        return success(ResultEnum.SUCCESS, data,data==null?0:1);
    }
    /**返回List*/
    public static ResultBean successList(Collection data) {
        return success(ResultEnum.SUCCESS, data,data.size());
    }public static ResultBean successList(ResultEnum resultEnum,Collection data) {
        return success(resultEnum, data,data.size());
    }
}
