package com.jeesite.modules.restful.activity;

import com.jeesite.common.bean.response.activity.AcContentResponse;
import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.utils.ResultUtil;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.activity.entity.ActivityContent;
import com.jeesite.modules.activity.service.ActivityContentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("interface/activity/content")
public class ActivityContentRestfulController extends BaseController {
    public static final Logger logger = LoggerFactory.getLogger(ActivityContentRestfulController.class);

    private final ActivityContentService activityContentService;

    @Autowired
    public ActivityContentRestfulController(ActivityContentService activityContentService) {
        this.activityContentService = activityContentService;
    }

    @GetMapping("/{contentID}")
    public ResultBean find(@PathVariable("contentID") String id) {
        ActivityContent activityContent = activityContentService.get(id);
        if (activityContent == null) {
            return ResultUtil.error("内容不存在");
        }
        return ResultUtil.successBody(new AcContentResponse().doCopier(activityContent,false));
    }
    @GetMapping("/info/list")
    public ResultBean findAll() {
        List<ActivityContent> list = activityContentService.findList(new ActivityContent());
        if (list == null) {
            return ResultUtil.error("内容不存在");
        }
        return ResultUtil.successList(new AcContentResponse().doCopiers(list,false));
    }
}
