package com.jeesite.common.bean.response.user;


import com.jeesite.common.bean.response.BaseResponse;
import com.jeesite.common.convert.user.AcUserConvert;
import com.jeesite.modules.user.entity.ActivityUser;

/**
 * 用于显示用户信息
 * @author ReStartLin
 */
public class AcUserResponse extends BaseResponse {
    private String id;
    private String openid;		// openid
    private String nickName;	// 用户名
    private String phone;		// 手机号
    private String role;		// 角色
    private String avatarUrl;	// wx-用户头像
    private String gender;		// 性别
    private String city;		// 城市
    private String province;	// 省份
    private String country;		// 国家
    private String language;	// 语言
    private String flag;		// 登录状态 1 在线 0 离线

    public AcUserResponse() {
        super(ActivityUser.class, AcUserResponse.class, new AcUserConvert());
    }

    @Override
    public BaseResponse instance() {
        return new AcUserResponse();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /***
     * getter setter
     */

    public String getOpenid() {
        return openid;
    }


    public String getNickName() {
        return nickName;
    }

    public String getPhone() {
        return phone;
    }

    public String getRole() {
        return role;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getGender() {
        return gender;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

    public String getLanguage() {
        return language;
    }

    public String getFlag() {
        return flag;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }


}
