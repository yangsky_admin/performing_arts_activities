/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.entity;


import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * activity_contentEntity
 * @author ReStartLin
 * @version 2018-09-17
 */
@Table(name="activity_content", alias="a", columns={
		@Column(name="id", attrName="id", label="内容id", isPK=true),
		@Column(name="activity_content", attrName="activityContent", label="富文本 base64"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class ActivityContent extends DataEntity<ActivityContent> {
	
	private static final long serialVersionUID = 1L;
	private String activityContent;		// 富文本 base64
	
	public ActivityContent() {
		this(null);
	}

	public ActivityContent(String id){
		super(id);
	}
	
	public String getActivityContent() {
		return activityContent;
	}

	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}
	
}