/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.service;

import java.util.List;

import com.jeesite.common.config.Global;
import com.jeesite.common.io.FileUtils;
import com.jeesite.common.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.user.entity.ActivityUserEntertainer;
import com.jeesite.modules.user.dao.ActivityUserEntertainerDao;

/**
 * activity_user_entertainerService
 * @author ReStartLin
 * @version 2018-10-06
 */
@Service
@Transactional(readOnly=true)
public class ActivityUserEntertainerService extends CrudService<ActivityUserEntertainerDao, ActivityUserEntertainer> {
	
	/**
	 * 获取单条数据
	 * @param activityUserEntertainer
	 * @return
	 */
	@Override
	public ActivityUserEntertainer get(ActivityUserEntertainer activityUserEntertainer) {
		return super.get(activityUserEntertainer);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param activityUserEntertainer
	 * @return
	 */
	@Override
	public Page<ActivityUserEntertainer> findPage(Page<ActivityUserEntertainer> page, ActivityUserEntertainer activityUserEntertainer) {
		return super.findPage(page, activityUserEntertainer);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param activityUserEntertainer
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(ActivityUserEntertainer activityUserEntertainer) {
		super.save(activityUserEntertainer);
	}
	
	/**
	 * 更新状态
	 * @param activityUserEntertainer
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(ActivityUserEntertainer activityUserEntertainer) {
		super.updateStatus(activityUserEntertainer);
	}
	
	/**
	 * 删除数据
	 * @param activityUserEntertainer
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(ActivityUserEntertainer activityUserEntertainer) {
		super.delete(activityUserEntertainer);
	}

	/**
	 * 保存头像
	 * @param imageBase64
	 * @param newEt
	 */
	@Transactional()
	public void saveAvatar(String imageBase64, ActivityUserEntertainer newEt) {
		if (StringUtils.isNoneBlank(imageBase64)) {
			if ("EMPTY".equals(imageBase64)) {
				newEt.setAvatar(StringUtils.EMPTY);
			} else {
				String imageUrl = "entertainer/avatar/" + newEt.getId() + "." +
						FileUtils.getFileExtensionByImageBase64(imageBase64);
				String fileName = Global.getUserfilesBaseDir(imageUrl);
				FileUtils.writeToFileByImageBase64(fileName, imageBase64);
				newEt.setAvatar("/js"+Global.USERFILES_BASE_URL+imageUrl);
			}
		}
	}
}