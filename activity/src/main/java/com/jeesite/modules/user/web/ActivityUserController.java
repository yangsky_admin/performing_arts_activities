/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.io.FileUtils;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.user.entity.ActivityUserEntertainer;
import com.jeesite.modules.user.service.ActivityUserEntertainerService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.user.entity.ActivityUser;
import com.jeesite.modules.user.service.ActivityUserService;

/**
 * activity_userController
 *
 * @author ReStartLin
 * @version 2018-10-06
 */
@Controller
@RequestMapping(value = "${adminPath}/user/activityUser")
public class ActivityUserController extends BaseController {

    private final ActivityUserService activityUserService;
    private final ActivityUserEntertainerService activityUserEntertainerService;

    @Autowired
    public ActivityUserController(ActivityUserService activityUserService, ActivityUserEntertainerService activityUserEntertainerService) {
        this.activityUserService = activityUserService;
        this.activityUserEntertainerService = activityUserEntertainerService;
    }


    /**
     * 获取数据
     */
    @ModelAttribute
    public ActivityUser get(String id, boolean isNewRecord) {
        return activityUserService.get(id, isNewRecord);
    }

    /**
     * 查询列表
     */
    @RequiresPermissions("user:activityUser:view")
    @RequestMapping(value = {"list", ""})
    public String list(ActivityUser activityUser, Model model) {
        model.addAttribute("activityUser", activityUser);
        return "modules/user/activityUserList";
    }

    /**
     * 查询列表数据
     */
    @RequiresPermissions("user:activityUser:view")
    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<ActivityUser> listData(ActivityUser activityUser, HttpServletRequest request, HttpServletResponse response) {
        Page<ActivityUser> page = activityUserService.findPage(new Page<ActivityUser>(request, response), activityUser);
        return page;
    }

    /**
     * 查看编辑表单
     */
    @RequiresPermissions("user:activityUser:view")
    @RequestMapping(value = "form")
    public String form(ActivityUser activityUser, Model model) {
        model.addAttribute("activityUser", activityUser);
        return "modules/user/activityUserForm";
    }

    /**
     * 保存activity_user
     */
    @RequiresPermissions("user:activityUser:edit")
    @PostMapping(value = "save")
    @ResponseBody
    @Transactional
    public String save(@Validated ActivityUser activityUser, HttpServletRequest request) {
        activityUserService.save(activityUser);
        if (activityUser.getRole().equals("2")) {
            String et_accessNumber = request.getParameter("et_accessNumber");
            String et_city = request.getParameter("et_city");
            String et_age = request.getParameter("et_age");
            String et_cityCode = request.getParameter("et_cityCode");
            String imageBase64 = request.getParameter("imageBase64");
            ActivityUserEntertainer newEt = new ActivityUserEntertainer();
            if (request.getParameter("et_id") != null) {
                newEt.setId(request.getParameter("et_id"));
            }
            newEt.setName(request.getParameter("et_name"));
            newEt.setCases(request.getParameter("et_cases"));
            newEt.setAccessNumber(StringUtils.isBlank(et_accessNumber) ? 0 : Long.valueOf(et_accessNumber));
            newEt.setShowPictures(request.getParameter("et_showPictures"));
            newEt.setCity(et_city);
            newEt.setPhone(request.getParameter("et_phone"));
            newEt.setGender(request.getParameter("et_gender"));
            newEt.setAge(StringUtils.isBlank(et_age) ? 0 : Integer.valueOf(et_age));
            newEt.setCityCode(et_cityCode);
            newEt.setPresentation(request.getParameter("et_presentation"));
            newEt.setLabel(request.getParameter("et_label"));
            newEt.setRemarks(request.getParameter("et_remarks"));
            newEt.setUserId(activityUser.getId());
            activityUserEntertainerService.save(newEt);
            //保存头像
            activityUserEntertainerService.saveAvatar(imageBase64, newEt);
            activityUserEntertainerService.update(newEt);
        }
        return renderResult(Global.TRUE, text("保存activity_user成功！"));
    }


    /**
     * 删除activity_user
     */
    @RequiresPermissions("user:activityUser:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(ActivityUser activityUser) {
        activityUserService.delete(activityUser);
        return renderResult(Global.TRUE, text("删除activity_user成功！"));
    }


}