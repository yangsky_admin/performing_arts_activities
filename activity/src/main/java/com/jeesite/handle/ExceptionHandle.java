package com.jeesite.handle;

import com.jeesite.common.bean.exception.ActivityUserException;
import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.bean.result.ResultEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolationException;

import static com.jeesite.common.utils.ResultUtil.error;

@ControllerAdvice
@ResponseBody
public class ExceptionHandle {
    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

    @ExceptionHandler(value = ActivityUserException.class)
    public ResultBean handle(Exception e) {
        logger.error("error:{}", e);
        if (e instanceof ConstraintViolationException) {
            return error(ResultEnum.ERROR_VIOLATION.getCode(), e.getMessage(), null,0);
        }
        return error(e.getMessage());
    }
}
