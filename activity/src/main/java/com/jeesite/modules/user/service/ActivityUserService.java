/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.service;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.user.entity.ActivityUser;
import com.jeesite.modules.user.dao.ActivityUserDao;

/**
 * activity_userService
 * @author ReStartLin
 * @version 2018-10-06
 */
@Service
@Transactional(readOnly=true)
public class ActivityUserService extends CrudService<ActivityUserDao, ActivityUser> {
	
	/**
	 * 获取单条数据
	 * @param activityUser
	 * @return
	 */
	@Override
	public ActivityUser get(ActivityUser activityUser) {
		return super.get(activityUser);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param activityUser
	 * @return
	 */
	@Override
	public Page<ActivityUser> findPage(Page<ActivityUser> page, ActivityUser activityUser) {
		return super.findPage(page, activityUser);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param activityUser
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(ActivityUser activityUser) {
		super.save(activityUser);
	}
	
	/**
	 * 更新状态
	 * @param activityUser
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(ActivityUser activityUser) {
		super.updateStatus(activityUser);
	}
	
	/**
	 * 删除数据
	 * @param activityUser
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(ActivityUser activityUser) {
		super.delete(activityUser);
	}
	
}