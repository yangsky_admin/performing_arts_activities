package com.jeesite.common.bean.response.activity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.bean.response.BaseResponse;
import com.jeesite.common.convert.activity.AcConvert;
import com.jeesite.modules.activity.entity.Activity;

import java.util.Date;

public class AcResponse extends BaseResponse {
    private String id;
    private String activityTitle;		// 标题
    private String activityType;		// 类别
    private String activityLocation;		// 位置
    private String activityContentId;		// 内容ID
    private String activitySmimg;		// 图片(缩略图)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date activityTime;		// 活动时间
    private Long activityHits;		// 阅读次数

    public AcResponse() {
        super(Activity.class, AcResponse.class, new AcConvert());
    }

    @Override
    public BaseResponse instance() {
        return new AcResponse();
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivityLocation() {
        return activityLocation;
    }

    public void setActivityLocation(String activityLocation) {
        this.activityLocation = activityLocation;
    }

    public String getActivityContentId() {
        return activityContentId;
    }

    public void setActivityContentId(String activityContentId) {
        this.activityContentId = activityContentId;
    }

    public String getActivitySmimg() {
        return activitySmimg;
    }

    public void setActivitySmimg(String activitySmimg) {
        this.activitySmimg = activitySmimg;
    }

    public Date getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(Date activityTime) {
        this.activityTime = activityTime;
    }

    public Long getActivityHits() {
        return activityHits;
    }

    public void setActivityHits(Long activityHits) {
        this.activityHits = activityHits;
    }
}
