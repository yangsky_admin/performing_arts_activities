package com.jeesite.modules.restful.user;

import com.jeesite.common.bean.response.user.AcUserEtResponse;
import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.bean.result.ResultEnum;
import com.jeesite.common.entity.Page;
import com.jeesite.common.utils.ResultUtil;
import com.jeesite.modules.user.entity.ActivityUserEntertainer;
import com.jeesite.modules.user.service.ActivityUserEntertainerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/interface/activity/user/et")
public class ActivityUserEntertainerRestfulController {
    public static final Logger logger = LoggerFactory.getLogger(ActivityUserEntertainerRestfulController.class);

    private final ActivityUserEntertainerService service;

    @Autowired
    public ActivityUserEntertainerRestfulController(ActivityUserEntertainerService service) {
        this.service = service;
    }

    /**
     * 根据userid获得演艺用户信息
     *
     * @return
     */
    @GetMapping("/info/{userId}")
    private ResultBean getUserEtInfoByUserId(@PathVariable(value = "userId") String id) {
        ActivityUserEntertainer activityUserEntertainer = new ActivityUserEntertainer();
        activityUserEntertainer.setUserId(id);
        List<ActivityUserEntertainer> list = service.findList(activityUserEntertainer);
        if (list != null && list.size() > 0) {
            return ResultUtil.successBody(new AcUserEtResponse().doCopier(list.get(0)));
        }
        return ResultUtil.error("获取信息失败,用户不存在");
    }

    /**
     * 根据id获得演艺用户信息
     *
     * @return
     */
    @GetMapping("/{id}")
    private ResultBean getUserEtInfoById(@PathVariable("id") String id) {
        ActivityUserEntertainer activityUserEntertainer1 = service.get(id);
        if (activityUserEntertainer1 != null) {
            return ResultUtil.successBody(new AcUserEtResponse().doCopier(activityUserEntertainer1));
        }
        return ResultUtil.error("获取信息失败,用户不存在");
    }

    /**
     * 获得所有的用户
     *
     * @return
     */
    @GetMapping("/list")
    public ResultBean queryAll() {
        List<ActivityUserEntertainer> list = service.findList(new ActivityUserEntertainer());
        if (list == null) {
            return ResultUtil.error(ResultEnum.USER_ERROR);
        }
        return ResultUtil.successList(new AcUserEtResponse().doCopiers(list));
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param size
     * @return
     */
    @GetMapping("/list/{pageIndex}/{size}")
    public ResultBean queryEts(@PathVariable("pageIndex") Integer pageIndex, @PathVariable("size") Integer size) {
        Page<ActivityUserEntertainer> page = service.findPage(new Page<>(pageIndex, size), new ActivityUserEntertainer());
        if (pageIndex > page.getLast()) {
            return ResultUtil.successList(ResultEnum.PAGE_EXCEPTION, new ArrayList());
        }
        List<ActivityUserEntertainer> list = page.getList();
        if (list == null) {
            return ResultUtil.error(ResultEnum.USER_ERROR);
        }
        return ResultUtil.successList(new AcUserEtResponse().doCopiers(list));
    }
}
