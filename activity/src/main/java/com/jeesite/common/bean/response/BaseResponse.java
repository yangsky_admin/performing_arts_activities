package com.jeesite.common.bean.response;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.core.Converter;
import java.util.ArrayList;
import java.util.List;

/**
 * copy bean
 * @author ReStartLin
 */
public abstract class BaseResponse {
    private Class sourceClazz; //源 class
    private Class targetClazz; //目标 class
    private Converter converter;//convert
    public BaseResponse(Class sourceClazz, Class targetClazz, Converter converter) {
        this.sourceClazz = sourceClazz;
        this.targetClazz = targetClazz;
        this.converter = converter;
    }
    /** 获取子类实例 */
    public abstract BaseResponse instance();
    /**  拷贝属性  */
    public <T> BaseResponse doCopier(T source, boolean useConvert) {
        BeanCopier beanCopier = BeanCopier.create(sourceClazz, targetClazz, useConvert);
        beanCopier.copy(source, this, converter);
        return this;
    }
    public <T> BaseResponse doCopier(T source) {  return doCopier(source,true); }
    /**  拷贝集合属性 */
    public <T> List<BaseResponse> doCopiers(List<T> sources, boolean useConvert) {
        List<BaseResponse> temps = new ArrayList<>();
        if (sources != null) {
            for (T source : sources) {
                BaseResponse baseResponse = instance().doCopier(source, useConvert);
                temps.add(baseResponse);
            }
        }
        return temps;
    }
    public <T> List<BaseResponse> doCopiers(List<T> sources) {
        return doCopiers(sources, true);
    }
}
