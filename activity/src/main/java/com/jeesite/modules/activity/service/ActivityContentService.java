/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.service;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.activity.entity.ActivityContent;
import com.jeesite.modules.activity.dao.ActivityContentDao;

/**
 * activity_contentService
 * @author ReStartLin
 * @version 2018-09-17
 */
@Service
@Transactional(readOnly=true)
public class ActivityContentService extends CrudService<ActivityContentDao, ActivityContent> {
	
	/**
	 * 获取单条数据
	 * @param activityContent
	 * @return
	 */
	@Override
	public ActivityContent get(ActivityContent activityContent) {
		return super.get(activityContent);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param activityContent
	 * @return
	 */
	@Override
	public Page<ActivityContent> findPage(Page<ActivityContent> page, ActivityContent activityContent) {
		return super.findPage(page, activityContent);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param activityContent
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(ActivityContent activityContent) {
		super.save(activityContent);
	}
	
	/**
	 * 更新状态
	 * @param activityContent
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(ActivityContent activityContent) {
		super.updateStatus(activityContent);
	}
	
	/**
	 * 删除数据
	 * @param activityContent
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(ActivityContent activityContent) {
		super.delete(activityContent);
	}

	
}