package com.jeesite.common.bean.result;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ResultBean {
    private int code;
    private Boolean error;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer total;
    private String msg;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object data;
    public ResultBean() {
    }

    public ResultBean(int code, String msg, Object data, Boolean error, Integer total) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.error = error;
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }
}
