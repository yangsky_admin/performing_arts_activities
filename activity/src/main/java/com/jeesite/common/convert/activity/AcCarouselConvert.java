package com.jeesite.common.convert.activity;

import com.jeesite.modules.sys.utils.DictUtils;
import org.springframework.cglib.core.Converter;

public class AcCarouselConvert implements Converter {
    public static final String AC_CL_TYPE = "ac_cl_type";
    @Override
    public Object convert(Object value, Class aClass, Object o1) {
        switch (o1.toString()) {
            case "setType":
                return DictUtils.getDictLabel(AC_CL_TYPE, value.toString(), "未知类型");
        }
        return value;
    }
}
