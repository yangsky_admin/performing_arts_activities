package com.jeesite.modules.restful.activity;

import com.jeesite.common.bean.response.activity.AcCarouselResponse;
import com.jeesite.common.bean.result.ResultBean;
import com.jeesite.common.bean.result.ResultEnum;
import com.jeesite.common.entity.Page;
import com.jeesite.common.utils.ResultUtil;
import com.jeesite.modules.activity.entity.ActivityCarousel;
import com.jeesite.modules.activity.service.ActivityCarouselService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/interface/activity/carousel")
public class ActivityCarouselRestfulController {

    public static final Logger logger = LoggerFactory.getLogger(ActivityCarouselRestfulController.class);

    private final ActivityCarouselService service;

    @Autowired
    public ActivityCarouselRestfulController(ActivityCarouselService service) {
        this.service = service;
    }


    /**
     * 获得所有的轮播信息 status = 0
     * @return
     */
    @GetMapping("/list")
    public ResultBean queryAll() {
        ActivityCarousel activityCarousel = new ActivityCarousel();
        List<ActivityCarousel> list = service.findList(activityCarousel);
        return ResultUtil.successList(new AcCarouselResponse().doCopiers(list));
    }

    /**
     * 分页查询获得所有的轮播信息 status = 0
     * pageIndex:第几页 从1开始
     * size:分页长度
     * @return
     */
    @GetMapping("/list/{pageIndex}/{size}")
    public ResultBean queryActivities(@PathVariable("pageIndex")Integer pageIndex, @PathVariable("size")Integer size) {
        ActivityCarousel activityCarousel = new ActivityCarousel();
        Page<ActivityCarousel> page = service.findPage(new Page<>(pageIndex, size), activityCarousel);
        if (pageIndex > page.getLast()) {
            return ResultUtil.successList(ResultEnum.PAGE_EXCEPTION, new ArrayList());
        }
        List<ActivityCarousel> list = page.getList();
        return ResultUtil.successList(new AcCarouselResponse().doCopiers(list));
    }

    /**
     * 增加阅读次数
     * @param id
     */
    @GetMapping("/{activityCarouselId}/hits")
    public ResultBean addHits(@PathVariable("activityCarouselId")String id) {
        ActivityCarousel activityCarousel = service.get(id);
        if (activityCarousel != null) {
            Integer hits = activityCarousel.getHits();
            activityCarousel.setHits(++hits);
            service.save(activityCarousel);
        }
        return ResultUtil.success();
    }
}
