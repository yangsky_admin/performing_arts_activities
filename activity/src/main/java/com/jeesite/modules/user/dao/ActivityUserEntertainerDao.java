/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.user.entity.ActivityUserEntertainer;

/**
 * activity_user_entertainerDAO接口
 * @author ReStartLin
 * @version 2018-10-06
 */
@MyBatisDao
public interface ActivityUserEntertainerDao extends CrudDao<ActivityUserEntertainer> {
	
}