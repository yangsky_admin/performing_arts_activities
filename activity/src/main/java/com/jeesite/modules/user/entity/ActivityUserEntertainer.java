/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

import javax.validation.constraints.Size;

/**
 * activity_user_entertainerEntity
 * @author ReStartLin
 * @version 2018-10-06
 */
@Table(name="activity_user_entertainer", alias="a", columns={
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(name="name", attrName="name", label="名字", queryType=QueryType.LIKE),
		@Column(name="phone", attrName="phone", label="手机号"),
		@Column(name="gender", attrName="gender", label="性别"),
		@Column(name="age", attrName="age", label="年龄"),
		@Column(name="avatar", attrName="avatar", label="头像"),
		@Column(name="show_pictures", attrName="showPictures", label="展示图片", comment="展示图片(使用'|'作为分隔符)"),
		@Column(name="city_code", attrName="cityCode", label="服务城市"),
		@Column(name="city", attrName="city", label="服务城市"),
		@Column(name="label", attrName="label", label="标签"),
		@Column(name="cases", attrName="cases", label="案例id关联"),
		@Column(name="presentation", attrName="presentation", label="艺人介绍"),
		@Column(name="access_number", attrName="accessNumber", label="访问数"),
		@Column(name="user_id", attrName="userId", label="关联的用户id"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class ActivityUserEntertainer extends DataEntity<ActivityUserEntertainer> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 名字
	private String phone;		// 手机号
	private String gender;		// 性别
	private Integer age;		// 年龄
	private String avatar;		// 头像
	private String showPictures;		// 展示图片(使用'|'作为分隔符)
	private String cityCode;		// 服务城市
	private String city;		// 服务城市
	private String label;		// 标签
	private String cases;		// 案例id关联
	private String presentation;		// 艺人介绍
	private Long accessNumber;		// 访问数
	private String userId;		// 关联的用户id
	
	public ActivityUserEntertainer() {
		this(null);
	}

	public ActivityUserEntertainer(String id){
		super(id);
	}
	
	@Length(min=0, max=25, message="名字长度不能超过 25 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=11, message="手机号长度不能超过 11 个字符")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=0, max=1, message="性别长度不能超过 1 个字符")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	@Length(min=0, max=255, message="头像长度不能超过 255 个字符")
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	@Length(min=0, max=255, message="展示图片长度不能超过 255 个字符")
	public String getShowPictures() {
		return showPictures;
	}

	public void setShowPictures(String showPictures) {
		this.showPictures = showPictures;
	}
	
	@Length(min=0, max=60, message="服务城市长度不能超过 60 个字符")
	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	@Length(min=0, max=100, message="服务城市长度不能超过 100 个字符")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@Length(min=0, max=255, message="标签长度不能超过 255 个字符")
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	@Length(min=0, max=255, message="案例id关联长度不能超过 255 个字符")
	public String getCases() {
		return cases;
	}

	public void setCases(String cases) {
		this.cases = cases;
	}
	
	public String getPresentation() {
		return presentation;
	}

	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}
	
	public Long getAccessNumber() {
		return accessNumber;
	}

	public void setAccessNumber(Long accessNumber) {
		this.accessNumber = accessNumber;
	}
	
	@Length(min=0, max=255, message="关联的用户id长度不能超过 255 个字符")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}