package com.jeesite.common.bean.response.activity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.bean.response.BaseResponse;
import com.jeesite.common.convert.activity.AcContentConvert;
import com.jeesite.modules.activity.entity.ActivityContent;

import java.util.Date;

public class AcContentResponse extends BaseResponse {
    private String id;
    private String activityContent;		// 富文本 base64
    protected String createBy;
    protected String updateBy;
    protected String remarks;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    protected Date createDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    protected Date updateDate;

    public AcContentResponse() {
        super(ActivityContent.class, AcContentResponse.class, new AcContentConvert());
    }

    @Override
    public BaseResponse instance() {
        return new AcContentResponse();
    }

    public String getActivityContent() {
        return activityContent;
    }

    public void setActivityContent(String activityContent) {
        this.activityContent = activityContent;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
