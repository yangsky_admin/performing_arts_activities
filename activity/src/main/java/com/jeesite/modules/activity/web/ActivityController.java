/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.activity.entity.ActivityContent;
import com.jeesite.modules.activity.service.ActivityContentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.activity.entity.Activity;
import com.jeesite.modules.activity.service.ActivityService;

import java.util.List;

/**
 * activityController
 * @author ReStartLin
 * @version 2018-09-28
 */
@Controller
@RequestMapping(value = "${adminPath}/activity/activity")
public class ActivityController extends BaseController {

	private final ActivityService activityService;
	private final ActivityContentService activityContentService;

	@Autowired
	public ActivityController(ActivityService activityService, ActivityContentService activityContentService) {
		this.activityService = activityService;
		this.activityContentService = activityContentService;
	}

	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Activity get(String id, boolean isNewRecord) {
		return activityService.get(id, isNewRecord);
	}

	/**
	 * 查询列表
	 */
	@RequiresPermissions("activity:activity:view")
	@RequestMapping(value = {"list", ""})
	public String list(Activity activity, Model model) {
		model.addAttribute("activity", activity);
		return "modules/activity/activityList";
	}

	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("activity:activity:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Activity> listData(Activity activity, HttpServletRequest request, HttpServletResponse response) {
		Page<Activity> page = activityService.findPage(new Page<>(request, response), activity);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("activity:activity:view")
	@RequestMapping(value = "form")
	public String form(Activity activity, Model model) {
		model.addAttribute("activity", activity);
		String activityContentId = activity.getActivityContentId();
		model.addAttribute("activityContent", "未编辑");
		if (!StringUtils.isEmpty(activityContentId)) {
			//根据内容ID获得富文本框的内容
			ActivityContent activityContent = activityContentService.get(activityContentId);
			if (activityContent != null) {
				String content = activityContent.getActivityContent();
				model.addAttribute("activityContent",content );
			}
		}
		return "modules/activity/activityForm";
	}

	/**
	 * 保存activity
	 */
	@RequiresPermissions("activity:activity:edit")
	@PostMapping(value = "save")
	@ResponseBody
	@Transactional
	public String save(HttpServletRequest request,@Validated Activity activity) {
		String contentEditor = request.getParameter("contentEditor");
		//富文本信息获得
		if (!StringUtils.isEmpty(contentEditor)){
			boolean flag = false;//是否是新的  true  是
			ActivityContent activityContent = new ActivityContent();
			activityContent.setActivityContent(contentEditor);
			if (!StringUtils.isEmpty(activity.getActivityContentId())) {
				activityContent.setId(activity.getActivityContentId());
				flag = true;
			}
			activityContentService.save(activityContent);
			if (!flag) {
				activity.setActivityContentId(activityContent.getId());
			}
		}
		//处理权重
		Long weight = activity.getWeight();
		if (weight == null) {
			Page<Activity> page = activityService.findPage(new Page<>(1, 1), new Activity());
			if (page != null) {
				List<Activity> list = page.getList();
				if (list != null && list.size() > 0) {
					activity.setWeight(list.get(0).getWeight()+1);
				}
			}
		}
		activityService.save(activity);
		return renderResult(Global.TRUE, text("保存activity成功！"));
	}

	/**
	 * 删除activity
	 */
	@RequiresPermissions("activity:activity:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Activity activity) {
		activityService.delete(activity);
		return renderResult(Global.TRUE, text("删除activity成功！"));
	}

}