/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.entity;


import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * activity_carouselEntity
 * @author ReStartLin
 * @version 2018-09-28
 */
@Table(name="activity_carousel", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="titile", attrName="titile", label="标题", queryType=QueryType.LIKE),
		@Column(name="weight", attrName="weight", label="权重"),
		@Column(name="type", attrName="type", label="类型"),
		@Column(name="activity_content_id", attrName="activityContentId", label="内容id"),
		@Column(name="img_url", attrName="imgUrl", label="图片相对路径"),
		@Column(name="activity_location_code", attrName="activityLocationCode", label="位置"),
		@Column(name="activity_location", attrName="activityLocation", label="位置", queryType=QueryType.LIKE),
		@Column(name="activity_time", attrName="activityTime", label="时间"),
		@Column(name="hits", attrName="hits", label="阅读次数"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.weight DESC"
)
public class ActivityCarousel extends DataEntity<ActivityCarousel> {
	
	private static final long serialVersionUID = 1L;
	private String titile;		// 标题
	private Long weight;		// 权重
	private String type;		// 类型
	private String activityContentId;		// 内容id
	private String imgUrl;		// 图片相对路径
	private String activityLocationCode;		// 位置
	private String activityLocation;		// 位置
	private Date activityTime;		// 时间
	private Integer hits;		// 阅读次数
	
	public ActivityCarousel() {
		this(null);
	}

	public ActivityCarousel(String id){
		super(id);
	}
	
	@NotBlank(message="标题不能为空")
	@Length(min=0, max=25, message="标题长度不能超过 25 个字符")
	public String getTitile() {
		return titile;
	}

	public void setTitile(String titile) {
		this.titile = titile;
	}
	
	public Long getWeight() {
		return weight;
	}

	public void setWeight(Long weight) {
		this.weight = weight;
	}
	
	@NotBlank(message="类型不能为空")
	@Length(min=0, max=25, message="类型长度不能超过 25 个字符")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getActivityContentId() {
		return activityContentId;
	}

	public void setActivityContentId(String activityContentId) {
		this.activityContentId = activityContentId;
	}
	
	@NotBlank(message="图片相对路径不能为空")
	@Length(min=0, max=255, message="图片相对路径长度不能超过 255 个字符")
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	@Length(min=0, max=64, message="位置长度不能超过 64 个字符")
	public String getActivityLocationCode() {
		return activityLocationCode;
	}

	public void setActivityLocationCode(String activityLocationCode) {
		this.activityLocationCode = activityLocationCode;
	}
	
	@Length(min=0, max=100, message="位置长度不能超过 100 个字符")
	public String getActivityLocation() {
		return activityLocation;
	}

	public void setActivityLocation(String activityLocation) {
		this.activityLocation = activityLocation;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="时间不能为空")
	public Date getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(Date activityTime) {
		this.activityTime = activityTime;
	}
	
	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}
	
}