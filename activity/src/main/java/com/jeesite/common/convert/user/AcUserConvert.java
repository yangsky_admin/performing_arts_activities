package com.jeesite.common.convert.user;

import org.springframework.cglib.core.Converter;

public class AcUserConvert implements Converter {
    @Override
    public Object convert(Object value, Class target, Object o1) {
        switch (o1.toString()) {
            case "setFlag":
                return value.equals("1") ? "在线" : "离线";
            case "setRole":
                switch (value.toString()) {
                    case "1":
                        return "普通用户";
                    case "2":
                        return "演艺人员";
                }
        }
        return value;
    }
}
