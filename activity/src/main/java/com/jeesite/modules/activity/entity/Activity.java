/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.entity;


import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * activityEntity
 * @author ReStartLin
 * @version 2018-09-28
 */
@Table(name="activity", alias="a", columns={
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(name="activity_title", attrName="activityTitle", label="标题", queryType=QueryType.LIKE),
		@Column(name="weight", attrName="weight", label="权重"),
		@Column(name="activity_type", attrName="activityType", label="类别"),
		@Column(name="activity_location_code", attrName="activityLocationCode", label="位置"),
		@Column(name="activity_location", attrName="activityLocation", label="位置", queryType=QueryType.LIKE),
		@Column(name="activity_content_id", attrName="activityContentId", label="内容ID"),
		@Column(name="activity_smimg", attrName="activitySmimg", label="图片", comment="图片(缩略图)"),
		@Column(name="activity_time", attrName="activityTime", label="活动时间"),
		@Column(name="activity_hits", attrName="activityHits", label="阅读次数"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.weight DESC"
)

public class Activity extends DataEntity<Activity> {
	
	private static final long serialVersionUID = 1L;
	private String activityTitle;		// 标题
	private Long weight;		// 权重
	private String activityType;		// 类别
	private String activityLocationCode;		// 位置
	private String activityLocation;		// 位置
	private String activityContentId;		// 内容ID
	private String activitySmimg;		// 图片(缩略图)
	private Date activityTime;		// 活动时间
	private Long activityHits;		// 阅读次数
	
	public Activity() {
		this(null);
	}

	public Activity(String id){
		super(id);
	}
	
	@NotBlank(message="标题不能为空")
	@Length(min=0, max=255, message="标题长度不能超过 255 个字符")
	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}
	
	public Long getWeight() {
		return weight;
	}

	public void setWeight(Long weight) {
		this.weight = weight;
	}
	
	@NotBlank(message="类别不能为空")
	@Length(min=0, max=100, message="类别长度不能超过 100 个字符")
	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	
	@Length(min=0, max=64, message="位置长度不能超过 64 个字符")
	public String getActivityLocationCode() {
		return activityLocationCode;
	}

	public void setActivityLocationCode(String activityLocationCode) {
		this.activityLocationCode = activityLocationCode;
	}
	
	@Length(min=0, max=100, message="位置长度不能超过 100 个字符")
	public String getActivityLocation() {
		return activityLocation;
	}

	public void setActivityLocation(String activityLocation) {
		this.activityLocation = activityLocation;
	}
	
	public String getActivityContentId() {
		return activityContentId;
	}

	public void setActivityContentId(String activityContentId) {
		this.activityContentId = activityContentId;
	}
	
	@Length(min=0, max=255, message="图片长度不能超过 255 个字符")
	public String getActivitySmimg() {
		return activitySmimg;
	}

	public void setActivitySmimg(String activitySmimg) {
		this.activitySmimg = activitySmimg;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="活动时间不能为空")
	public Date getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(Date activityTime) {
		this.activityTime = activityTime;
	}
	
	public Long getActivityHits() {
		return activityHits;
	}

	public void setActivityHits(Long activityHits) {
		this.activityHits = activityHits;
	}

}