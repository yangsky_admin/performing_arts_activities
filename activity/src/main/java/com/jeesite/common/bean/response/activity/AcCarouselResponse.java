package com.jeesite.common.bean.response.activity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.bean.response.BaseResponse;
import com.jeesite.common.convert.activity.AcCarouselConvert;
import com.jeesite.modules.activity.entity.ActivityCarousel;

import java.util.Date;

public class AcCarouselResponse extends BaseResponse {
    private String id;
    private String titile;		// 标题
    private String type;		// 类型
    private String activityContentId;		// 内容id
    private String imgUrl;		// 图片相对路径
    private String activityLocation;		// 位置
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date activityTime;		// 时间
    private Integer hits;		// 阅读次数

    public AcCarouselResponse() {
        super(ActivityCarousel.class, AcCarouselResponse.class, new AcCarouselConvert());
    }

    @Override
    public BaseResponse instance() {
        return new AcCarouselResponse();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitile() {
        return titile;
    }

    public void setTitile(String titile) {
        this.titile = titile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActivityContentId() {
        return activityContentId;
    }

    public void setActivityContentId(String activityContentId) {
        this.activityContentId = activityContentId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getActivityLocation() {
        return activityLocation;
    }

    public void setActivityLocation(String activityLocation) {
        this.activityLocation = activityLocation;
    }

    public Date getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(Date activityTime) {
        this.activityTime = activityTime;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }
}
