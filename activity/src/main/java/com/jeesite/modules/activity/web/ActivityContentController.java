/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.activity.entity.ActivityContent;
import com.jeesite.modules.activity.service.ActivityContentService;

/**
 * activity_contentController
 * @author ReStartLin
 * @version 2018-09-17
 */
@Controller
@RequestMapping(value = "${adminPath}/activity/activityContent")
public class ActivityContentController extends BaseController {

	@Autowired
	private ActivityContentService activityContentService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public ActivityContent get(String id, boolean isNewRecord) {
		return activityContentService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("activity:activityContent:view")
	@RequestMapping(value = {"list", ""})
	public String list(ActivityContent activityContent, Model model) {
		model.addAttribute("activityContent", activityContent);
		return "modules/activity/activityContentList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("activity:activityContent:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<ActivityContent> listData(ActivityContent activityContent, HttpServletRequest request, HttpServletResponse response) {
		Page<ActivityContent> page = activityContentService.findPage(new Page<ActivityContent>(request, response), activityContent); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("activity:activityContent:view")
	@RequestMapping(value = "form")
	public String form(ActivityContent activityContent, Model model) {
		model.addAttribute("activityContent", activityContent);
		return "modules/activity/activityContentForm";
	}

	/**
	 * 保存activity_content
	 */
	@RequiresPermissions("activity:activityContent:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated ActivityContent activityContent) {
		activityContentService.save(activityContent);
		return renderResult(Global.TRUE, text("保存activity_content成功！"));
	}
	
	/**
	 * 删除activity_content
	 */
	@RequiresPermissions("activity:activityContent:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(ActivityContent activityContent) {
		activityContentService.delete(activityContent);
		return renderResult(Global.TRUE, text("删除activity_content成功！"));
	}

}