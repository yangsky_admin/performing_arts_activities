package com.jeesite.common.bean.response.user;

import com.jeesite.common.bean.response.BaseResponse;
import com.jeesite.common.convert.user.AcUserEtConvert;
import com.jeesite.modules.user.entity.ActivityUserEntertainer;

public class AcUserEtResponse extends BaseResponse {
    private String id;
    private String name;		// 名字
    private String phone;		// 手机号
    private String gender;		// 性别
    private Integer age;		// 年龄
    private String avatar;		// 头像
    private String showPictures;		// 展示图片(使用'|'作为分隔符)
    private String city;		// 服务城市
    private String label;		// 标签
    private String cases;		// 案例id关联
    private String presentation;// 艺人介绍
    private Long accessNumber;		// 访问数
    private String userId;		// 关联的用户id
    public AcUserEtResponse() {
        super(ActivityUserEntertainer.class, AcUserEtResponse.class, new AcUserEtConvert());
    }

    @Override
    public BaseResponse instance() {
        return new AcUserEtResponse();
    }
    /**
     * getter setter
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getShowPictures() {
        return showPictures;
    }

    public void setShowPictures(String showPictures) {
        this.showPictures = showPictures;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCases() {
        return cases;
    }

    public void setCases(String cases) {
        this.cases = cases;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public Long getAccessNumber() {
        return accessNumber;
    }

    public void setAccessNumber(Long accessNumber) {
        this.accessNumber = accessNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
