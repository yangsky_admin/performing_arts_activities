/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.entity;

import org.hibernate.validator.constraints.Length;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

import javax.validation.constraints.NotBlank;

/**
 * activity_userEntity
 * @author ReStartLin
 * @version 2018-10-06
 */
@Table(name="activity_user", alias="a", columns={
		@Column(name="id", attrName="id", label="用户ID", isPK=true),
		@Column(name="openid", attrName="openid", label="openid"),
		@Column(name="password", attrName="password", label="密码"),
		@Column(name="nick_name", attrName="nickName", label="用户名", queryType=QueryType.LIKE),
		@Column(name="phone", attrName="phone", label="手机号"),
		@Column(name="role", attrName="role", label="角色"),
		@Column(name="avatar_url", attrName="avatarUrl", label="wx-用户头像"),
		@Column(name="gender", attrName="gender", label="性别"),
		@Column(name="city", attrName="city", label="城市"),
		@Column(name="province", attrName="province", label="省份"),
		@Column(name="country", attrName="country", label="国家"),
		@Column(name="language", attrName="language", label="语言"),
		@Column(name="flag", attrName="flag", label="登录状态 1 在线 0 离线"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class ActivityUser extends DataEntity<ActivityUser> {
	
	private static final long serialVersionUID = 1L;
	private String openid;		// openid
	private String password;		// 密码
	private String nickName;		// 用户名
	private String phone;		// 手机号
	private String role;		// 角色
	private String avatarUrl;		// wx-用户头像
	private String gender;		// 性别
	private String city;		// 城市
	private String province;		// 省份
	private String country;		// 国家
	private String language;		// 语言
	private String flag;		// 登录状态 1 在线 0 离线
	
	public ActivityUser() {
		this(null);
	}

	public ActivityUser(String id){
		super(id);
	}
	
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	@Length(min=0, max=11, message="手机号长度不能超过 11 个字符")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@NotBlank(message="角色不能为空")
	@Length(min=0, max=1, message="角色长度不能超过 1 个字符")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	@Length(min=0, max=1, message="登录状态 1 在线 0 离线长度不能超过 1 个字符")
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}