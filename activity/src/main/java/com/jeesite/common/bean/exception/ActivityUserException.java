package com.jeesite.common.bean.exception;

public class ActivityUserException extends Exception {

    public ActivityUserException(String s) {
        super(s);
    }

    public ActivityUserException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ActivityUserException(Throwable throwable) {
        super(throwable);
    }

    public ActivityUserException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

    public ActivityUserException() {
    }
}
