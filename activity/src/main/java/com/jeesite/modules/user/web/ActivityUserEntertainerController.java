/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.druid.support.json.JSONUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.io.FileUtils;
import com.jeesite.common.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.user.entity.ActivityUserEntertainer;
import com.jeesite.modules.user.service.ActivityUserEntertainerService;

import java.util.List;

/**
 * activity_user_entertainerController
 * @author ReStartLin
 * @version 2018-10-06
 */
@Controller
@RequestMapping(value = "${adminPath}/user/activityUserEntertainer")
public class ActivityUserEntertainerController extends BaseController {

	@Autowired
	private ActivityUserEntertainerService activityUserEntertainerService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public ActivityUserEntertainer get(String id, boolean isNewRecord) {
		return activityUserEntertainerService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("user:activityUserEntertainer:view")
	@RequestMapping(value = {"list", ""})
	public String list(ActivityUserEntertainer activityUserEntertainer, Model model) {
		model.addAttribute("activityUserEntertainer", activityUserEntertainer);
		return "modules/user/activityUserEntertainerList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("user:activityUserEntertainer:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<ActivityUserEntertainer> listData(ActivityUserEntertainer activityUserEntertainer, HttpServletRequest request, HttpServletResponse response) {
		Page<ActivityUserEntertainer> page = activityUserEntertainerService.findPage(new Page<ActivityUserEntertainer>(request, response), activityUserEntertainer); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("user:activityUserEntertainer:view")
	@RequestMapping(value = "form")
	public String form(ActivityUserEntertainer activityUserEntertainer, Model model) {
		model.addAttribute("activityUserEntertainer", activityUserEntertainer);
		return "modules/user/activityUserEntertainerForm";
	}

	/**
	 * 保存activity_user_entertainer
	 */
	@RequiresPermissions("user:activityUserEntertainer:edit")
	@PostMapping(value = "save")
	@ResponseBody
	@Transactional
	public String save(@Validated ActivityUserEntertainer activityUserEntertainer, HttpServletRequest request) {
        activityUserEntertainerService.save(activityUserEntertainer);
		String imageBase64 = request.getParameter("imageBase64");
		//保存头像
		activityUserEntertainerService.saveAvatar(imageBase64, activityUserEntertainer);
		activityUserEntertainerService.update(activityUserEntertainer);
		return renderResult(Global.TRUE, text("保存演艺用户信息成功！"));
	}
	
	/**
	 * 删除activity_user_entertainer
	 */
	@RequiresPermissions("user:activityUserEntertainer:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(ActivityUserEntertainer activityUserEntertainer) {
		activityUserEntertainerService.delete(activityUserEntertainer);
		return renderResult(Global.TRUE, text("删除演艺用户信息成功！"));
	}

    /**
     * getEtInfo
     */
    @RequestMapping(value = "getEtInfo")
    @ResponseBody
    public String test(String userId) {
        ActivityUserEntertainer query = new ActivityUserEntertainer();
        query.setUserId(userId);
        List<ActivityUserEntertainer> list = activityUserEntertainerService.findList(query);
        if (list != null && list.size() > 0) {
            return renderResult("成功", text("200"),list.get(0));
        }
        return renderResult("该用户不是演艺人员", text("400"));
    }
}