/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.user.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.user.entity.ActivityUser;

/**
 * activity_userDAO接口
 * @author ReStartLin
 * @version 2018-10-06
 */
@MyBatisDao
public interface ActivityUserDao extends CrudDao<ActivityUser> {
	
}