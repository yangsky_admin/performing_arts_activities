package com.jeesite.common.convert.activity;

import com.jeesite.modules.sys.utils.DictUtils;
import org.springframework.cglib.core.Converter;

public class AcConvert implements Converter {
    public static final String ACTIVITY_TYPE = "ac_type";
    @Override
    public Object convert(Object value, Class aClass, Object o1) {
        switch (o1.toString()) {
            case "setActivityType":
                return DictUtils.getDictLabel(ACTIVITY_TYPE, value.toString(), "未知类型");

        }
        return value;
    }
}
