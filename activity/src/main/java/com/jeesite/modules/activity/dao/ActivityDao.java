/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.activity.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.activity.entity.Activity;

/**
 * activityDAO接口
 * @author ReStartLin
 * @version 2018-09-28
 */
@MyBatisDao
public interface ActivityDao extends CrudDao<Activity> {
	
}